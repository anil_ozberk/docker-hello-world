FROM openjdk:8
RUN mkdir app
ADD App.jar app/App.jar
WORKDIR app
RUN "pwd"
RUN "ls"
ENTRYPOINT ["java","-jar", "App.jar"]